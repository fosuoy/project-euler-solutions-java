This is a project intended to be deployed on Openshift to showcase S2i
capabilities.


The backend is springboot (can be created / deployed using standard Java), the
front-end is intended to be deployed in a JBOSS eap container.


Instruction to deploy - 


 - `oc cluster up`
 - `oc new project testing`
 - `oc import-image registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift --confirm`
 - `oc import-image registry.access.redhat.com/jboss-eap-7/eap71-openshift --confirm`
 - `oc new-app oc new-app openjdk18-openshift~https://bitbucket.org/fosuoy/project-euler-solutions-java.git --context-dir=backend-spring --name=euler-backend`
 - `oc new-app oc new-app eap71-openshift~https://bitbucket.org/fosuoy/project-euler-solutions-java.git --context-dir=frontend-jboss --name=frontend-jboss`
 - `oc expose svc frontend-jboss`
 - `...wait for it to be build / deployed`
 - `oc get route`


... go to the URL found in the routes above and you'll see the app!
