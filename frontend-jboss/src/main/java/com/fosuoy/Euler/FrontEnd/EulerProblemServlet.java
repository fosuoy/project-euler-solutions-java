package com.fosuoy.Euler.FrontEnd;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@SuppressWarnings("serial")
@WebServlet({"/problem_001/*", "/problem_002/*", "/problem_003/*", "/problem_028/*"})

public class EulerProblemServlet extends HttpServlet {

    static String PAGE_HEADER = "<html><head><title>Problem!</title></head><body>";

    static String PAGE_FOOTER = "</body></html>";

    EulerProblem eulerProblem = new EulerProblem();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String problem = req.getServletPath();

        String problemText = "";
        try {
            problemText = eulerProblem.getIndividualProblem(problem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println(PAGE_HEADER);
        writer.println("<h3>Solution:</h3><br><p>" + problemText + "</p>");
        writer.println(PAGE_FOOTER);
        writer.close();
    }
}
