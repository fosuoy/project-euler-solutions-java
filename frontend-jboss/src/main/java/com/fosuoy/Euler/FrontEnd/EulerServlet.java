package com.fosuoy.Euler.FrontEnd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/Euler")

public class EulerServlet extends HttpServlet{

    static String PAGE_HEADER = "<html><head><title>Problem Index</title></head><body>";

    static String PAGE_FOOTER = "</body></html>";

    EulerIndex eulerIndex = new EulerIndex();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String indexText = null;
        String indexBlurb = "<h2>This is a front-end to display solutions to problems from:" + 
            "<br><a href=\"http://projecteuler.net/\">Project Euler</a></h2><h3><br>" +
            "Solutions are calculated then returned from a SpringBoot micro-service.<br>" + 
            "If you see null below, it means the backend hasn't been deployed correctly.</h3><br>";
        try {
            indexText = eulerIndex.createIndexList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println(PAGE_HEADER);
        writer.println(indexBlurb + "<h1>Available problems:</h1><br><p>" + indexText + "</p>");
        writer.println(PAGE_FOOTER);
        writer.close();
    }

}
