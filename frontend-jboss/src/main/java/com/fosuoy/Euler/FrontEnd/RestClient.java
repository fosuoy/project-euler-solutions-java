package com.fosuoy.Euler.FrontEnd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.google.gson.Gson;

public class RestClient {

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URLConnection url = new URL(urlString).openConnection();
            url.setConnectTimeout(1000);
            url.setReadTimeout(1000);
            reader = new BufferedReader(new InputStreamReader(url.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    public static List<String> getIndexList() throws Exception {

        String json = readUrl("http://euler-backend:8080/");

        Gson gson = new Gson();
        Index index = gson.fromJson(json, Index.class);
        List<String> indexList = index.current_problems;

        return indexList;
    }

    public static String getIndexProblems() throws Exception {
        List<String> index_list = getIndexList();
        String text = "";
        for (String problem : index_list) {
            text += "<a href=\"" + problem + "\">" + problem + "</a><br>";
        }
        return text;
    }

    public static String getIndividualProblems(String problemNo) throws Exception {

        String json = readUrl("http://euler-backend:8080" + problemNo);

        Gson gson = new Gson();
        Problem problem = gson.fromJson(json, Problem.class);

        String text = "";
        text += "Link: <a href=\"" + problem.url +"\">" + problemNo + "</a><br>";
        text += "Question: <br>" + problem.blurb + "<br>";
        text += "Answer: <br>" + problem.answer + "<br>";
        return text;
    }

}
