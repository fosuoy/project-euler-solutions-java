package com.fosuoy.Euler.FrontEnd;

import java.util.List;

public class EulerIndex
{
	String createIndexList() throws Exception {
		return RestClient.getIndexProblems();
	}

	public List<String> getIndexList() throws Exception {
		return RestClient.getIndexList();
	}
}
