package com.fosuoy.euler.problems.problemsets;
import com.fosuoy.euler.problems.problems;

public class problem_003 implements problems {

    public String blurb = "The prime factors of 13195 are: <br>" +
                          "5, 7, 13 and 29. <br>" +
                          "What is the largest prime factor of the number: <br>" +
                          "600851475143? <br>";

    public String url = "https://projecteuler.net/problem=3";

    private Long LIMIT = 600851475143L;

    private Long result(Long VARIABLE) {
        int Factor = 2;
        while ( Factor < VARIABLE) {
            while ( VARIABLE % Factor == 0 ) {
                VARIABLE = VARIABLE / Factor ;
            }
            Factor = Factor + 1;
        }
        return VARIABLE;
    }

    public String answer = String.valueOf( result(LIMIT) );

}
