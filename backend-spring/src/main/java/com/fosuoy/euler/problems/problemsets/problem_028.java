package com.fosuoy.euler.problems.problemsets;

import java.util.Arrays;

public class problem_028 {

    public String blurb = "Starting with the number 1 and moving to the right " +
            "in a clockwise direction a 5 by 5 spiral is formed as follows:<br>" +
            "<br>" +
            "21 22 23 24 25<br>" +
            "20  7  8  9 10<br>" +
            "19  6  1  2 11<br>" +
            "18  5  4  3 12<br>" +
            "17 16 15 14 13<br>" +
            "<br>" +
            "It can be verified that the sum of the numbers on the diagonals is 101.<br>" +
            "<br>" +
            "What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?<br>";

    public String url = "https://projecteuler.net/problem=28";

    private int LIMIT = 1001;

    private int result(int VARIABLE) {
        // Form spiral number pattern
        int[][] spiral;
        spiral = new int[VARIABLE][VARIABLE];
        int midPointX = VARIABLE / 2;
        int midPointY = VARIABLE / 2;
        int x = 0;
        int y = 0;
        int dx = 0;
        int dy = -1;
        for (int i = 1; i < (VARIABLE * VARIABLE) + 1 ; i++) {
            if ( -(VARIABLE/2)<=x && -(VARIABLE/2)<=y && x<=(VARIABLE/2) && y<=(VARIABLE/2) ) {
                int coOrdinateX = midPointX + x;
                int coOrdinateY = midPointY + y;
                spiral[coOrdinateY][coOrdinateX] = i;
            }
            if ( (x == y) || (x < 0 && x == -y) || (x > 0 && x == 1 - y) ) {
                int dyOld = dy;
                int dxOld = dx;
                dx = -dyOld;
                dy = dxOld;
            }
            x += dx;
            y += dy;
        }

        // Count up diagonal numbers on each line
        int position = 0;
        int spiralDiagnal = 0;
        int midPoint = spiral.length / 2;
        for (int i = 0; i < spiral.length; i++ ) {
            int[] line = spiral[i];

            if (i < midPoint) {
                int lineLength = line.length - 1;
                spiralDiagnal += line[position] + line[lineLength - position];
                position += 1;
                continue;
            }
            if (i == midPoint) {
                spiralDiagnal += line[position];
                position -= 1;
                continue;
            }
            if (i > midPoint) {
                int lineLength = line.length - 1;
                spiralDiagnal += line[position] + line[lineLength - position];
                position -= 1;
                continue;
            }
        }

        return spiralDiagnal;
    }

    public String answer = String.valueOf( result(LIMIT) );
}
