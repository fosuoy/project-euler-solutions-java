package com.fosuoy.euler.problems.problemsets;

import com.fosuoy.euler.problems.problems;

import java.util.ArrayList;

public class problem_001 implements problems {

  public String blurb = "If we list all the natural numbers below 10 that are " +
                   "multiples of 3 or 5, we get 3, 5, 6 and 9. The sum " + 
                   "of these multiples is 23.<br> " +
                   "Find the sum of all the multiples of 3 or 5 below 1000.<br>";

  public String url = "https://projecteuler.net/problem=1";

  private int LIMIT = 1000;

  private int result(int VARIABLE) {
    ArrayList<Integer> multiplesList = new ArrayList<Integer>();
    int count = 0;
    while (count < VARIABLE) {
      if (count % 3 == 0 || count % 5 == 0) {
        multiplesList.add(count);
      }
      count += 1;
    }
    int sum = multiplesList.stream().mapToInt(Integer::intValue).sum();
    return sum;
  }

  public String answer = String.valueOf( result(LIMIT) );

}

