package com.fosuoy.euler;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fosuoy.euler.problems.problemsets.problem_001;
import com.fosuoy.euler.problems.problemsets.problem_002;
import com.fosuoy.euler.problems.problemsets.problem_003;
import com.fosuoy.euler.problems.problemsets.problem_028;

import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.List;

@RestController
public class Controller {

    @RequestMapping("/")
    public Map<String, List> index() {
        HashMap<String, List> index = new HashMap();
        List<String> urls = Arrays.asList(
                "/problem_001",
                "/problem_002",
                "/problem_003",
                "/problem_028");
        index.put("current_problems", urls);
        return index;
    }

    @RequestMapping("/problem_001")
    public Map<String, String> problem_001_REST() {
        HashMap<String, String> result = new HashMap();

        problem_001 problem = new problem_001();

        result.put("url", problem.url);
        result.put("blurb", problem.blurb);
        result.put("answer", problem.answer );

        return result;
    }

    @RequestMapping("/problem_002")
    public Map<String, String> problem_002_REST() {
        HashMap<String, String> result = new HashMap();

        problem_002 problem = new problem_002();

        result.put("url", problem.url);
        result.put("blurb", problem.blurb);
        result.put("answer", problem.answer );

        return result;
    }

    @RequestMapping("/problem_003")
    public Map<String, String> problem_003_REST() {
        HashMap<String, String> result = new HashMap();

        problem_003 problem = new problem_003();

        result.put("url", problem.url);
        result.put("blurb", problem.blurb);
        result.put("answer", problem.answer );

        return result;
    }

    @RequestMapping("/problem_028")
    public Map<String, String> problem_028_REST() {
        HashMap<String, String> result = new HashMap();

        problem_028 problem = new problem_028();

        result.put("url", problem.url);
        result.put("blurb", problem.blurb);
        result.put("answer", problem.answer );

        return result;
    }

}
